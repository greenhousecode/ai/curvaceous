# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [v0.2.0] - 2021-10-21

### Added

- Methods `__rmul__` and `scale` to scale a curve horizontally and vertically.

## [v0.1.0] - UNKNOWN

### Added

- The ``Curve`` and ``Interval`` classes
- The ``maximize`` method to solve the budget-constrained optimization problem
  over a list of curves.
