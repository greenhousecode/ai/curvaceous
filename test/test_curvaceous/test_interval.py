import numpy as np

from curvaceous.interval import Interval


class TestInterval:
    def test_interval_contains_its_endpoints(self):
        interval = Interval(0, 1)
        for x in np.linspace(0, 1, 100):
            assert x in interval

    def test_closest_maps_to_boundary_point(self):
        interval = Interval(0, 1)
        assert interval.closest(1.5) == 1
        assert interval.closest(-1.5) == 0
