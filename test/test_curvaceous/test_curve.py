import pytest
import numpy as np

from curvaceous.curve import Curve
from curvaceous.interval import Interval


def is_close(x, y, epsilon=0.01) -> bool:
    return abs(x - y) < x * epsilon


class TestCurve:
    def test_create_curve_from_lists(self):
        try:
            Curve([1, 2], [3, 4])
        except Exception as exc:  # pylint: disable=broad-except
            assert False, exc

    def test_create_curve_from_ndarrays(self):
        try:
            Curve(np.array([1, 2]), np.array([3, 4]))
        except Exception as exc:  # pylint: disable=broad-except
            assert False, exc

    def test_fail_creation_on_unequal_length_inputs(self):
        with pytest.raises(ValueError):
            Curve([1, 2], [3, 4, 5])

    def test_len_on_curve_gives_number_of_knots(self):
        assert len(Curve([1, 2], [3, 4])) == 2

    def test_create_curve_from_dict(self):
        try:
            Curve.from_dict({"xs": [1, 2], "ys": [3, 4]})
        except Exception as exc:  # pylint: disable=broad-except
            assert False, exc

    def test_domain_returns_interval_of_definition(self):
        curve = Curve([1, 2, 10], [5, 4, 3])
        assert curve.domain == Interval(1, 10)

    def test_evaluating_curve_at_knot_yields_y_value(self):
        xs = [1, 2, 3, 4]
        ys = [5, 6, 7, 8]
        curve = Curve(xs, ys)
        for (x, y) in zip(xs, ys):
            assert curve(x) == y

    def test_evaluating_curve_between_knots_yields_interpolated_value(self):
        curve = Curve([1, 2, 3, 4], [5, 6, 7, 8])
        assert curve(1.5) == 5.5
        assert curve(2.7) == 6.7
        assert curve(3.1) == 7.1

    def test_evaluating_curve_outside_domain_returns_none(self):
        curve = Curve([1, 2, 3, 4], [5, 6, 7, 8])
        assert curve(0) is None
        assert curve(9) is None

    def test_evaluating_curve_outside_domain_returns_closest_when_cut(self):
        curve = Curve([1, 2, 3, 4], [5, 6, 7, 8])
        assert curve(0, cut=True) == 5
        assert curve(9, cut=True) == 8

    def test_adding_two_curves_give_sum_curve(self):
        curve_a = Curve([1, 3], [0, 1])
        curve_b = Curve([1, 2, 3], [2, 5, 6])
        curve = curve_a + curve_b
        xs = np.arange(1.0, 3.1, 0.5)
        for x in xs:
            assert curve_a(x) is not None, x
            assert curve_b(x) is not None, x
            assert curve(x) == curve_a(x) + curve_b(x), x

    def test_resampling_returns_a_curve_of_length_the_sample(self):
        curve = Curve(np.linspace(1, 10, 10), np.linspace(1, 10, 10))
        cs = np.linspace(1, 10, 100)
        actual = curve.resample(cs)
        assert len(actual) == len(cs)

    def test_rmul_with_non_scalar_fails(self):
        curve = Curve(np.linspace(1, 10, 10), np.linspace(1, 10, 10))
        with pytest.raises(TypeError):
            # pylint: disable=pointless-statement
            "hello" * curve

    @pytest.mark.parametrize("factor", [0.1, 0.5, 2, 4, 10])
    def test_rmul_with_scalar_multiplies_y_values(self, factor):
        curve = Curve(np.linspace(1, 10, 10), np.linspace(1, 10, 10))
        actual = factor * curve
        for x in np.linspace(1, 10, 100):
            assert is_close(actual(x), factor * curve(x))

    def test_scale_with_non_scalar_fails(self):
        curve = Curve(np.linspace(1, 10, 10), np.linspace(1, 10, 10))
        with pytest.raises(TypeError):
            # pylint: disable=pointless-statement
            curve.scale("hello")

    @pytest.mark.parametrize("factor", [0.1, 0.5, 2, 4, 10])
    def test_scale_with_scalar_stretches_the_curve(self, factor):
        curve = Curve(np.linspace(1, 10, 10), np.linspace(1, 10, 10))
        actual = curve.scale(factor)
        for x in np.linspace(1 / factor, 10 / factor, 100):
            assert is_close(actual(x), curve(factor * x))


# TODO: The implementation is faulty
# class TestMaxWithConstant:
#     def test_max_with_constant_evaluates_correctly(self):
#         curve = Curve(np.linspace(1, 10, 10), [4, -3, -10, 100, 1, 3, 4, -5, 8, 10])
#         actual = max_with_constant(0, curve)
#         for x in np.linspace(1, 10, 100):
#             assert is_close(actual(x), max(curve(x), 0)), x
