# pylint: disable=unused-import
from curvaceous.curve import max_with_constant, Curve
from curvaceous.solvers import maximize
from curvaceous.interval import Interval, equidistant
